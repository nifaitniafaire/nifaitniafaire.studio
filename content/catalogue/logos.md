---
title: "logos"
date: 2019-05-09
showDate: false
tags: ["3D", "plastiques"]
images:
    - /catalogue/logos2.jpg
---

![I](/catalogue/logos1.png)

Simulation d'effondrement d'une sphère en papier, bleu sur jaune fondamental NCS.

[GitLab](https://gitlab.com/nifaitniafaire/logos)