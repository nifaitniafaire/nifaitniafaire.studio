---
title: "Échos"
date: 2019-01-01
showDate: false
tags: ["photo"]
images:
    - /catalogue/echoes16.jpg
---

Macrophotographie, pop art patrimonialisé (fresque *Kiekeboe*, Théâtre Américain de Laeken. Ardilla A., Kuleczko R., Oreopoulos G., Vandegeerde D. d'après Merho)


![1](/catalogue/echoes1.jpg)


![2](/catalogue/echoes2.jpg)


![3](/catalogue/echoes3.jpg)


![4](/catalogue/echoes4.jpg)


![5](/catalogue/echoes5.jpg)


![6](/catalogue/echoes6.jpg)


![7](/catalogue/echoes7.jpg)


![8](/catalogue/echoes8.jpg)


![9](/catalogue/echoes9.jpg)


![10](/catalogue/echoes10.jpg)


![11](/catalogue/echoes11.jpg)


![12](/catalogue/echoes12.jpg)


![13](/catalogue/echoes13.jpg)


![14](/catalogue/echoes14.jpg)


![15](/catalogue/echoes15.jpg)


![16](/catalogue/echoes16.jpg)


[GitLab](https://gitlab.com/nifaitniafaire/name-and-fame)
