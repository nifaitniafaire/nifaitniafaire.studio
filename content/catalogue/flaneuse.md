---
title: "Flâneuse in Paradise"
date: 2019-06-09
showDate: false
tags: ["photo"]
images:
    - /catalogue/flaneuse04.JPG
---

Entre les aéroports de Roissy et du Bourget, le projet Europacity ambitionne d’ériger 80 hectares de structures commerciales et touristiques à l’horizon 2024. À l'heure des ZAD et de la lutte contre les « grands projets inutiles », cette initiative se voit opposer ses impacts écologiques et sociaux. Depuis 2011, mobilisation militante et recours juridiques entravent l’avancée du projet.

En 2017, les travaux de Xavier Boissel et Didier Vivien m'avaient guidée jusqu’au futur emplacement d’Europacity, sur les traces de fantomatiques vestiges de la Première Guerre mondiale. J'y suis retournée en mars 2019, après l’annulation du plan local d'urbanisme associé au projet. Il s’agissait pour moi d’explorer ces sites : le Triangle de Gonesse, espace agricole bordé d’infrastructures de transport (autoroutes, aéroports) et strié de pylônes électriques ; le parc d'activités Paris-Nord 2, enclave commerciale et industrielle au milieu des champs ; un territoire sans habitants permanents, siège d’une intense activité économique le jour mais déserté la nuit. Un non-lieu anonymisé, dont l’insularité préfigure celle d’Europacity.

Je voulais rendre compte de l’étrangeté de cet endroit, dessiner les contours d’un imaginaire capitaliste délocalisé, émanation rurale de la métropole parisienne. Des jours et nuits durant, je me suis laissée porter par mes pas, dérivant dans ces paysages humanisés mais vides.
J’ai ainsi pu observer la cohabitation, parfois conflictuelle, entre deux usages de l'espace : celui d’un hub logistique et économique et celui d’une agriculture de production.

J'ai voulu éviter le récit d'une "France périphérique" abstraite de divisions sociales, de même que j’ai pris garde à ne pas mettre en scène une opposition entre Homme et nature : de nature ici, il n’y a qu’une présence géométrisée et contrôlée (agriculture intensive, parc naturel aménagé). À ce conflit, j’ai préféré la concurrence entre deux modes de production.

Ces contradictions se veulent résolues dans le projet irénique d'un Grand Paris entrepreneurial, mais les affrontements en cours laissent, pour l’instant, présager du contraire.


![1](/catalogue/flaneuse01.JPG)

![2](/catalogue/flaneuse02.JPG)

![3](/catalogue/flaneuse03.JPG)

![4](/catalogue/flaneuse04.JPG)

![5](/catalogue/flaneuse05.JPG)





[Série complète (pdf, 19 photographies)](https://f002.backblazeb2.com/file/nifaitniafaire-public/Flaneuse+in+Paradise.pdf), [GitLab](https://gitlab.com/nifaitniafaire/flaneuse-in-paradise)
