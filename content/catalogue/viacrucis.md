---
title: "via crucis feminae currentis"
date: 2018-06-09
showDate: false
tags: ["photo"]
images:
    - /catalogue/viacrucis2.png
---

## Meurtres de la joggeuse

**En tenue de sport, loin de son foyer, à l'aube ou au crépuscule**. Si les circonstances de sa disparition captivent, c'est que la joggeuse concentre les angoisses patriarcales. À chaque drame s'écrit une nécrologie punitive, qui substitue à l'assassinat d'une femme - souvent par un proche - les conséquences d'une transgression. Le fait divers vaut avertissement.


_via crucis feminae currentis_ explore cet imaginaire dans ses théâtres matériels : torche à la main, le parcours sportif Dausmenil est éclaté puis reconstruit en mosaïques numériques. Le mobilier urbain se fait expressionniste, le bois de Vincennes devient espace liminal – entre conte cruel et quotidienneté rassurante. Les aspérités logicielles en sont autant de dissonances.




![XI](/catalogue/viacrucis1.png)

![XIV](/catalogue/viacrucis2.png)





[Série complète (pdf, 14 photographies)](https://f002.backblazeb2.com/file/nifaitniafaire-public/via+crucis.pdf), [GitLab](https://gitlab.com/nifaitniafaire/via-crucis-feminae-currentis)
